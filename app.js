express = require('express');
app = require('express.io')();

app.http().io();
lodash = require('lodash');
apiai = require('apiai');
apiaiapp = apiai("631b4664b638416f8a31b4341b8b77bb");


// Express Middleware for serving static files
// app.use(express.static(path.join(__dirname, 'public')));
// first parameter is the mount point, second is the location in the file system
app.use("/public", express.static(__dirname + "/public"));
// Setup the ready route, and emit talk event.

app.io.route('ready', function(req) {

   socket_id = req.socket.id;
   room = socket_id;

   console.log('new room', room);
   console.log('new client', socket_id);

   req.io.join(room);
   
	var request = apiaiapp.textRequest('hackawacka01', {
		sessionId: socket_id
	});

	request.on('response', function(response) {
		console.log(response);

		req.io.emit('init', {
			msg: response.result.fulfillment.speech,
			from: 'BeBot'
		})
	});

	request.on('error', function(error) {
		console.log(error);
	});

	request.end();

})

// Send the client or admin html.
app.get('/', function(req, res) {
	    res.sendfile(__dirname + '/client.html')
	});

// 
// For clients
// 

app.io.route('disconnect', function(req) {
  console.warn('io.route.disconnect');
});

// Setup the message route for clients
app.io.route('talk', function(req) {
    var msg = req.data.msg;
    var from = req.data.from;
   socket_id = req.socket.id;
   room = socket_id;

	var request = apiaiapp.textRequest(msg, {
		sessionId: socket_id
	});

	request.on('response', function(response) {
		console.log(response);

		app.io.room(room).broadcast('got-message', {
			msg: response.result.fulfillment.speech,
			from: 'BeBot'
		});
	});

	request.on('error', function(error) {
		console.log(error);
	});

	request.end();

});

app.listen(process.env.PORT || 8080, function() {
  console.log('Example app listening on port '+ process.env.PORT)

});