# README #

This is Team PFURA's PLDT 88 2016 Hackathon repo for BeBot chatbot using NodeJS, ExpressJS, Socket.io, Express.io, AngularJS and API.ai.

### Requirements ###

Make sure you've installed the following:

NodeJS version 4.5

--------------------

After cloning this repo, do:

npm install

--------------------

To run the app in the browser:

node app.js

--------------------

### DEMO ###

Go to: http://pldt-bebot.herokuapp.com/